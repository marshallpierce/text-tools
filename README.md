A toy CLI tool to do some basic text manipulations.

To see subcommands:

```
cargo run -- help
```

For any subcommand, see help by passing `-h`.
