use std::io::{self, BufRead};

use rand::seq::SliceRandom;

use clap::{App, Arg, SubCommand};

fn main() {
    let app = App::new("text-tools")
        .subcommand(
            SubCommand::with_name("sample")
                .about("Sample a configurable number of lines from the input by buffering the entire input and then sampling")
                .arg(
                    Arg::with_name("num-lines")
                        .short("n")
                        .long("num-lines")
                        .value_name("NUMBER-OF-LINES")
                        .help("Number of lines to sample")
                        .takes_value(true)
                        .default_value("1"),
                )
        ).subcommand(
        SubCommand::with_name("sample-res")
            .about("Sample a configurable number of lines from the input using reservoir sampling")
            .arg(
                Arg::with_name("num-lines")
                    .short("n")
                    .long("num-lines")
                    .value_name("NUMBER-OF-LINES")
                    .help("Number of lines to sample")
                    .takes_value(true)
                    .default_value("1"),
            )
    ).subcommand(SubCommand::with_name("shuffle")
        .about("Permute all the lines of the input")
    ).subcommand(SubCommand::with_name("nl-every-n")
        .about("Insert a newline every n lines")
        .arg(
            Arg::with_name("num-lines")
                .short("n")
                .long("num-lines")
                .value_name("NUMBER-OF-LINES")
                .help("Number of lines between inserted newlines")
                .takes_value(true)
                .default_value("1"),
        )
    );

    let mut rng = rand::thread_rng();

    match app.get_matches().subcommand() {
        ("sample", Some(sub_matches)) => {
            let lines_to_sample = sub_matches
                .value_of("num-lines")
                .map(|s| s.parse::<usize>().unwrap())
                .expect("number of lines must be an int");

            // accumulate all lines, then sample
            &lines(io::stdin().lock()).collect::<Vec<String>>()[..]
                .choose_multiple(&mut rng, lines_to_sample)
                .for_each(|l| println!("{}", l));
        }
        ("sample-res", Some(sub_matches)) => {
            let lines_to_sample = sub_matches
                .value_of("num-lines")
                .map(|s| s.parse::<usize>().unwrap())
                .expect("number of lines must be an int");

            reservoir_sample(&mut lines(io::stdin().lock()), &mut rng, lines_to_sample)
                .iter()
                .for_each(|l| println!("{}", l));
        }
        ("shuffle", Some(_)) => {
            let mut lines = lines(io::stdin().lock()).collect::<Vec<String>>();
            &lines[..].shuffle(&mut rng);

            &lines[..].iter().for_each(|l| println!("{}", l));
        }
        ("nl-every-n", Some(sub_matches)) => {
            let lines_to_group = sub_matches
                .value_of("num-lines")
                .map(|s| s.parse::<usize>().unwrap())
                .expect("number of lines must be an int");

            io::stdin()
                .lock()
                .lines()
                .map(|r| r.unwrap())
                .enumerate()
                .for_each(|(index, line)|  {
                    println!("{}", line);

                    if (index + 1) % lines_to_group == 0 {
                        println!();
                    }
                }
            );
        }
        _ => panic!("No subcommand specified, see -h"),
    }
}

fn lines<R: BufRead>(r: R) -> impl Iterator<Item=String> {
    return r.lines().map(|r| r.unwrap());
}

fn reservoir_sample<I: Iterator, R: rand::Rng>(
    iter: &mut I,
    rng: &mut R,
    num: usize,
) -> Vec<I::Item> {
    let mut vec = Vec::with_capacity(num);

    for (index, item) in iter.enumerate() {
        if index < num {
            vec.push(item);
        } else {
            // range includes the current index
            let random_index = rng.gen_range(0, index + 1);
            if random_index < num {
                vec[random_index] = item;
            }
        }
    }

    return vec;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reservoir_sample_samples_everything_at_least_once() {
        let mut data = Vec::new();

        for n in 0..1000 {
            data.push(n);
        }

        // sample 10 values 1000 times. Each number in the original data is extremely likely to show
        // up at least once.

        let mut rng = rand::thread_rng();

        let mut all_samples = std::collections::HashSet::new();

        for _ in 0..1000 {
            let sample = reservoir_sample(&mut data.iter(), &mut rng, 10);
            for n in sample {
                all_samples.insert(n);
            }
        }

        assert_eq!(data.len(), all_samples.len());
    }
}
